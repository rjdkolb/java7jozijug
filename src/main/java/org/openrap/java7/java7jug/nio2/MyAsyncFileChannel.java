/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.nio2;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 *
 * @author richard
 */
public class MyAsyncFileChannel {

    public static void main(String[] args) {
        Path tmpBlaFile = Paths.get("/home/richard/tmp/fjug.txt");

        tmpBlaFile.toFile().delete();


        int fileOffset = 0;
        try (AsynchronousFileChannel file = AsynchronousFileChannel.open(tmpBlaFile, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.DELETE_ON_CLOSE)) {

            ByteBuffer buffer1 = ByteBuffer.wrap("Java 7 FJUG\n".getBytes());
            ByteBuffer buffer2 = ByteBuffer.wrap("AsynchronousFileChannel\n".getBytes());

            Future<Integer> dataWriten1 = file.write(buffer1, fileOffset);
            fileOffset += buffer1.array().length;
            Future<Integer> dataWriten2 = file.write(buffer2, fileOffset);
            fileOffset += buffer2.array().length;

            dataWriten1.get();
            dataWriten2.get();

        } catch (ExecutionException | InterruptedException | IOException ex) {
            ex.printStackTrace();
        }

    }
}
