/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.nio2;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author richard
 */
public class TestAsyncServerSocketGet implements CompletionHandler<AsynchronousSocketChannel, String> {

    public static void main(String[] args) {
        //AsynchronousServerSocketChannel chan = null;
        InetSocketAddress socket = new InetSocketAddress(1234);

        try (AsynchronousServerSocketChannel chan = AsynchronousServerSocketChannel.open().bind(socket)) {



            //AsynchronousSocketChannel worker = acceptFuture.get(20, TimeUnit.SECONDS);



            chan.accept(null, new TestAsyncServerSocketGet());

         
            Thread.sleep(10000);



        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Unable to bind " + ex.getMessage());
        }


    }

    @Override
    public void completed(AsynchronousSocketChannel result, String attachment) {

        ByteBuffer buffer = ByteBuffer.allocate(128);
        Future<Integer> dataRead = result.read(buffer);
        try {
            dataRead.get();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } catch (ExecutionException ex) {
            ex.printStackTrace();
        }
        System.out.println("<" + result + "> + " + attachment + " : " + String.valueOf(buffer));


        result.write(ByteBuffer.wrap("FJUG Telnet".getBytes()));

    }

    @Override
    public void failed(Throwable exc, String attachment) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
