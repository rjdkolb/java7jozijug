/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.nio2;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author richard
 */
public class WatchMyTemp {

    public static void main(String[] args) throws IOException, InterruptedException {
        WatchService watchService = FileSystems.getDefault().newWatchService();
        File watchDirFile = new File("/tmp/");
        Path watchDirPath = watchDirFile.toPath();

        WatchKey watchKey = watchDirPath.register(watchService,
                StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);


        for (int count = 0; count < 1000; count++) {

            WatchKey key = watchService.poll(100, TimeUnit.MILLISECONDS);
            for (WatchEvent<?> event : watchKey.pollEvents()) {
                
                System.out.println(
                        "An event was found after file creation of kind " + event.kind()
                        + ". The event occurred on file " + event.context() + ".");
                watchKey.reset();//cancel event
            }

        }


    }
}
