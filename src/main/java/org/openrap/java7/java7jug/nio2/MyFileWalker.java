/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.nio2;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 *
 * @author richard
 */
public class MyFileWalker extends SimpleFileVisitor<Path> {

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
            throws IOException {



        System.out.println("DIR <" + dir + ">");
        return FileVisitResult.CONTINUE;//FileVisitResult.SKIP_SUBTREE
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
            throws IOException {
        System.out.println("Visit " + file + " is link :" + attrs.isSymbolicLink());
        return FileVisitResult.CONTINUE;
    }

    public static void main(String[] args) {
        Path headDir = Paths.get("/home/richard/tmp");
        try {
            Files.walkFileTree(headDir, new MyFileWalker());
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
