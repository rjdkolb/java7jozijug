/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.nio2;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.StandardProtocolFamily;
import java.net.StandardSocketOptions;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author richard
 * 
 * On Linux : sudo tcpdump  -i wlan0 host 226.18.84.25

 * 
 */
public class MutiCastInJava7 {

    public static void main(String[] args) throws SocketException, UnknownHostException, InterruptedException {
        //AsynchronousServerSocketChannel chan = null;
        int mutiCastPort = 5239;
        InetAddress mutiCast = InetAddress.getByName("226.18.84.25");

        
        NetworkInterface nic = NetworkInterface.getByName("wlan0");
        
        try (DatagramChannel chan = DatagramChannel.open(StandardProtocolFamily.INET)) {



            //AsynchronousSocketChannel worker = acceptFuture.get(20, TimeUnit.SECONDS);
             chan.setOption(StandardSocketOptions.SO_REUSEADDR, true);
             
             chan.bind(new InetSocketAddress(mutiCastPort));
             
             chan.setOption(StandardSocketOptions.IP_MULTICAST_IF, nic);
             
             MembershipKey key = chan.join(mutiCast,nic);

             chan.send(ByteBuffer.wrap("FJUG Broadcast".getBytes()),new InetSocketAddress(mutiCast,mutiCastPort));
             
             final ByteBuffer rxBuffer = ByteBuffer.allocate(128);
             SocketAddress address = chan.receive(rxBuffer);
             
             System.out.println("Message from address "+address + " "+new String(rxBuffer.array()));
             
             Thread.sleep(20000);
             

        }  catch ( IOException ex) {
            System.out.println("Unable to bind " + ex.getMessage());
        }


    }
}
