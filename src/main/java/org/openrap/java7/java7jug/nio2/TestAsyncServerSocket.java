/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.nio2;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author richard
 */
public class TestAsyncServerSocket {

    public static void main(String[] args) {
        //AsynchronousServerSocketChannel chan = null;
        InetSocketAddress socket = new InetSocketAddress(1234);

        try (AsynchronousServerSocketChannel chan = AsynchronousServerSocketChannel.open().bind(socket)) {



            //AsynchronousSocketChannel worker = acceptFuture.get(20, TimeUnit.SECONDS);

             Future<AsynchronousSocketChannel> acceptFuture = chan.accept();
             AsynchronousSocketChannel data = acceptFuture.get();
             
             Future<Integer> futureWrite = data.write(ByteBuffer.wrap("Welcome to FJUG Telnet\n\n".getBytes()));
             futureWrite.get();//wait till write complete
            while (true) {

               
                
                ByteBuffer buffer = ByteBuffer.allocate(128);
                Future<Integer> dataRead = data.read(buffer);
                int dataReadBytes = dataRead.get();
                String dataString = new String(buffer.array());
                System.out.print("<" + dataString + "> "+ dataReadBytes);


                
            }


        } catch ( InterruptedException ex) {
            ex.printStackTrace();
        } catch (ExecutionException | IOException ex) {
            System.out.println("Unable to bind " + ex.getMessage());
        }


    }
}
