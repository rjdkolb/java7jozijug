/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.forkjoin;

import java.util.Arrays;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;

/**
 *
 * @author richard
 */
public class RandomProblemRecursiveAction extends RecursiveTask<Long>{
	private int[] list;
	public long result;

        private int THRESHOLD = 10;
        
	public RandomProblemRecursiveAction(int[] array) {
		this.list = array;
	}

	@Override
	protected Long compute() {

		if (list.length  <= THRESHOLD) {//threshold
//			result = list[0];
                    for (int i = 0; i < list.length; i++) {
                            result += list[i];
                    }                   
                        
                        
                        
		} else {
			int midpoint = list.length / 2;
			int[] l1 = Arrays.copyOfRange(list, 0, midpoint);
			int[] l2 = Arrays.copyOfRange(list, midpoint, list.length);
			RandomProblemRecursiveAction worker1 = new RandomProblemRecursiveAction(l1);
			RandomProblemRecursiveAction worker2 = new RandomProblemRecursiveAction(l2);
			
                        worker1.fork();
                        
                        result = worker2.compute() + worker1.join();

		}
                return result;
	}

    public long getResult() {
        return result;
    }
        
        
        
}
