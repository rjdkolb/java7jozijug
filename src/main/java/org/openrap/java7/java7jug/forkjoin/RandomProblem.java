/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.forkjoin;

import org.perf4j.StopWatch;

import java.util.Random;

/**
 * 
 * This class defines a long list of integers which defines the problem we will
 * later try to solve
 * 
 */
public class RandomProblem {

    private final int[] list = new int[2000000];

    public void calc() {
        Random generator = new Random(19580427);
        for (int i = 0; i < list.length; i++) {
            list[i] = generator.nextInt(500000);
        }
    }

    public int[] getList() {
        return list;
    }

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        RandomProblem p = new RandomProblem();
        p.calc();
        stopWatch.stop();


        System.out.println("Elapsed Time: " + stopWatch.getElapsedTime());


    }
}
