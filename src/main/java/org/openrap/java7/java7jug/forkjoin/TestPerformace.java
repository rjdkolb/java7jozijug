/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.forkjoin;

import java.util.concurrent.ForkJoinPool;
import org.perf4j.StopWatch;

/**
 *
 * @author richard
 */
public class TestPerformace {
	public static void main(String[] args) {
		RandomProblem test = new RandomProblem();
                
                test.calc();
                
		// Check the number of available processors
		int nThreads = Runtime.getRuntime().availableProcessors();
		System.out.println(nThreads);
		RandomProblemRecursiveAction mfj = new RandomProblemRecursiveAction(test.getList());
		StopWatch stopWatchForkJoin = new StopWatch();
                
                
                ForkJoinPool pool = new ForkJoinPool(nThreads*10);
		pool.invoke(mfj);
		long result = mfj.getResult();
                stopWatchForkJoin.stop();
                System.out.println("Elapsed Time (fork join): " + stopWatchForkJoin.getElapsedTime());
                
		System.out.println("Done. Result: " + result);
		System.out.println("");
                StopWatch stopWatchClassic = new StopWatch();
                
                long sum = 0;
		// Check if the result was ok
		for (int i = 0; i < test.getList().length; i++) {
			sum += test.getList()[i];
		}
                stopWatchClassic.stop();
                System.out.println("Elapsed Time (classic): " + stopWatchClassic.getElapsedTime());
		System.out.println("Done. Result: " + sum);
	}
}
