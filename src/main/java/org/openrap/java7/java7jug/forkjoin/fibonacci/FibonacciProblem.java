/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.forkjoin.fibonacci;

import org.perf4j.StopWatch;

/**
 *
 * @author richard
 */
public class FibonacciProblem {
 
 public int n;

 public FibonacciProblem(int n) {
  this.n = n;
 }
 
 public long solve() {
  return fibonacci(n);
 }
 
 private long fibonacci(int n) {
  //System.out.println("Thread: " +
 //  Thread.currentThread().getName() + " calculates " + n);
  if (n <= 1)
   return n;
  else 
   return fibonacci(n-1) + fibonacci(n-2);
 }

 public static void main(String []args){
     FibonacciProblem p = new FibonacciProblem(40);
     StopWatch stopWatch = new StopWatch();
     System.out.println(p.solve());
     stopWatch.stop();
     System.out.println(stopWatch.getElapsedTime() +" ms");
     
 }
 
 
}

