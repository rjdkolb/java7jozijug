/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.forkjoin.fibonacci;

import org.perf4j.StopWatch;

/**
 *
 * @author richard
 */
public class NormalJava {

    public static long fibonacci(long number) {
        if ((number == 0) || (number == 1)) {
            return number;
        } else {
            return fibonacci(number - 1) + fibonacci(number - 2);
        }
    }

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        System.out.printf("Fibonacci of %d is: %d\n",
                40, fibonacci(40));
        stopWatch.stop();
        System.out.println(stopWatch.getElapsedTime() + " ms");

    }
}
