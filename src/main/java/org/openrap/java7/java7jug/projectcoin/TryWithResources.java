/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.projectcoin;

import java.io.Closeable;
import java.io.IOException;

/**
 *
 * @author richard
 * Java 7 docs for AutoCloseable and Closeable
 * http://download.oracle.com/javase/7/docs/api/java/lang/AutoCloseable.html
 * http://download.oracle.com/javase/7/docs/api/java/io/Closeable.html

 * As well :
 * http://www.javacodegeeks.com/2011/07/java-7-try-with-resources-explained.html
 * http://fahdshariff.blogspot.com/2011/07/java-7-try-with-resources.html
 * 
 */
public class TryWithResources {

    public static void main(String[] args) {


        
        try (MyJava7AutoCloseable java7 = new MyJava7AutoCloseable()) {
            System.out.println("Main::MyJava7AutoCloseable START");
            throw new IOException("io");
        } catch (IOException ioExp) {
            System.out.println("Main::MyJava7AutoCloseable END "+ioExp.getMessage());
            
            ioExp.getSuppressed();
            
        } finally {
            
            
            System.out.println("Main::MyJava7AutoCloseable END");
        }

//        try (MyJava6Closeable java6 = new MyJava6Closeable()) {
//            System.out.println("Main::MyJava6Closeable START");
//        } catch (IOException ioExp) {
//            //won't happen in this test
//        } finally {
//            System.out.println("Main::MyJava6Closeable END");
//        }

    }
}

class MyJava7AutoCloseable implements AutoCloseable {

    @Override
    public void close() throws IOException {
        System.out.println("MyJava7AutoCloseable::close");
        
        throw new RuntimeException("adsfsdf");
    }
}

class MyJava6Closeable implements Closeable {

    @Override
    public void close() throws IOException {
        System.out.println("MyJava6Closeable::close");
    }
}
