/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.projectcoin;

/**
 *
 * @author richard
 */
public class Literals {

    public static void main(String[] args) {
        int x = 0B1101001110101;//binary
        System.out.println(x);

        int underScore1 = 12_300_000;
        int underScore2 = 1_23;
        System.out.println("Does underScore1 == underScore2 "+(underScore1 == underScore2));
        
        double exponent = 1_2e5;
        System.out.println("exponent is "+exponent);
        
    
    }
}
