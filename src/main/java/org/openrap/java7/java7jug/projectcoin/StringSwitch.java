/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.projectcoin;

/**
 *
 * @author richard
 */
public class StringSwitch {
    public static void main(String []args){
        
        String myJava = "Java 7";
        
        switch (myJava){
            case "Java 7" :
                System.out.println("7");
                break;
            case "Java 6" :
                System.out.println("6, you are ok");  
                break;
            case "Java 5" :
                System.out.println("5, please upgrade");  
                break;
            case "Java 4" :
                System.out.println("4, urg");  
                break;
        }
        
        
    }
}
