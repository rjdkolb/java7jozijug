/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openrap.java7.java7jug.projectcoin;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author richard
 */


public class VarArgWarnings {
    public static void main(String [] args) {

        
        printAll(new ArrayList<String>(), new LinkedList<String>());
    }
    
    //@SafeVarargs
    public static void printAll(List<String> ... args){
        for(List<String> arg:args){
            System.out.println(arg);
        }
    }
}
